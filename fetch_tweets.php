<?php

define('SCREEN_NAME', '');
define('DELAY_IN_SECONDS', 60 * 5);
$customerKey = '';
$customerSecret = '';
$discordWebhook = 'https://discordapp.com/api/webhooks/';
$sinceId = null;

do {
    echo date("Y-m-d H:i:s") . ' Check Start' . PHP_EOL;
    if (($token = fetchBearerToken($customerKey, $customerSecret)) !== FALSE) {
        if (($tweets = fetchTweet($token, $sinceId)) !== false && count($tweets) > 0) {
            echo date("Y-m-d H:i:s") . ' Posting to Webhook...' . PHP_EOL;
            foreach ($tweets as $tweet) {
                $sinceId = $tweet['id_str'];
                postToDiscord($tweet, $discordWebhook);
            }
        }
    }
    echo date("Y-m-d H:i:s") . ' Sleeping for ' . DELAY_IN_SECONDS . ' seconds.' . PHP_EOL;
    sleep(DELAY_IN_SECONDS);
} while (true);

/**
 * Fetches the working authorization bearer token for us to read the latest tweet.
 * @param string $customerKey
 * @param string $customerSecret
 * @reeturn string of the access token or FALSE if failed to do so.
 */
function fetchBearerToken($customerKey, $customerSecret) {
    $url = 'https://api.twitter.com/oauth2/token';
    $params = array(
        'grant_type' => 'client_credentials'
    );
    $header = array();
    $header[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
    $header[] = 'Authorization: Basic ' . base64_encode($customerKey . ':' . $customerSecret);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $response = curl_exec($ch);
    $data = json_decode($response, true);
    if (isset($data['access_token'])) {
        $retVal = $data['access_token'];
    } else {
        $retVal = FALSE;
    }
    return $retVal;
}

/**
 * Fetches the tweet by predefined hardcode.
 * @param string $bearerToken
 * @param string $sinceId Since when was the last tweet ID we've found.
 * @return array of the response or FALSE upon failure to get json response
 */
function fetchTweet($bearerToken, $sinceId = null) {
    $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    $params = array(
        'screen_name' => SCREEN_NAME,
        'count' => 2,
        'exclude_replies' => 'true',
        'include_rts' => 'false'
    );
    if ($sinceId != null) {
        $params['since_id'] = $sinceId;
    }
    $url .= '?' . http_build_query($params);
    $header = array();
    $header[] = 'Content-type: application/json; charset=utf-8';
    $header[] = 'Authorization: Bearer ' . $bearerToken;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPGET, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $response = curl_exec($ch);
    $data = json_decode($response, true);
    // Cheap way to check is the response a working json...
    if (is_array($data)) {
        $retVal = $data;
    } else {
        $retVal = FALSE;
    }
    return $retVal;
}

function postToDiscord($tweet, $webhook) {
    $url = $webhook;
    $params = array(
        'content' => 'https://twitter.com/' . SCREEN_NAME . '/status/' . $tweet['id_str']
    );
    $header = array();
    $header[] = 'Content-type: application/json;charset=UTF-8';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $response = curl_exec($ch);
}
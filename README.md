# PHP tweet fetcher

A script done in PHP to fetch the latest tweets from a specific timeline to be posted on a Discord channel via defined webhook address.
This saves the time and effort from the user to manually go and paste the same tweet to their Discord server.

## Using the script

1. Setup a Twitter developer account.

2. Create new Twitter app.

3. Fetch the consumer key and secret to be set for `$customerKey` and `$customerSecret` variable.

4. Set appropriate `Webhook URL`.

5. Run the script in separate screen job and let it run indefinitely.

## Use case

At the time of this writing, only one publisher/company is making use of this script actively.

## Known issue(s)

- On initial run, the script will fetch multiple tweets and have them posted to the webhook in very short time.
This behaviour will no longer be exhibited after the script becomes aware on what is the latest tweet's ID.